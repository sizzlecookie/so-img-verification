//=========================================
// set defaults for page validations
var fullNameValidated = 0;
var passwordValidated = 0;
var emailValidated = 0;
var uploadValidated = 0;
//=========================================

/*
 Function called when file input updated. If there is a file selected, then
 start upload procedure by asking for a signed request from the app.
 */
function initUpload() {
    const files = document.getElementById('file-type').files;
    const file = files[0];
    if (file == null) {
        return alert('No verification image uploaded, please upload and re-submit.');
    }
    $(".overlay").show(); // show loading img
    getSignedRequest(file);

    // kick off the phantom script
    memberfulUpload();
}

/*
 Function to get the temporary signed request from the app.
 If request successful, continue to upload the file using this signed
 request.
 */
function getSignedRequest(file) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', '/s3_img_upload_signed_request?file-name=' +
        document.getElementById("form_full_name").value + '_' +
        document.getElementById("form_email").value
        + '&file-type=' + file.type);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                uploadFile(file, response.signedRequest, response.url);
            }
            else {
                alert('Could not get signed URL, please try again later or contact sellerolympus@gmail.com for support.');
            }
        }
    };
    xhr.send();
}

//Function to carry out the actual PUT request to S3 using the signed request from the app.
function uploadFile(file, signedRequest, url) {
    const xhr = new XMLHttpRequest();
    xhr.open('PUT', signedRequest);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
            }
            else {
                alert('Could not upload file, please try again later or contact sellerolympus@gmail.com for support.');
            }
        }
    };
    xhr.send(file);
}

/*
 Confirm email is available w/ Memberful
 */
function emailAvailability() {
    const email = document.getElementById('form_email').value;
    const xhr = new XMLHttpRequest();
    xhr.open('GET',
        '/email_availability?form_email='
        + email);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                if (!response.passed) {
                    alert("Account exists with this email!\nPlease enter new email or visit sellerolympus.com to login");
                    return;
                }
                console.log(response.passed);
            }
        }
    };
    xhr.send();
}

// get request to initiate phantomjs form submission
function memberfulUpload() {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', '/submit_to_memberful?form_full_name=' +
        document.getElementById("form_full_name").value + '&form_email=' +
        document.getElementById("form_email").value + '&form_password=' +
        document.getElementById("form_password").value);  //+ '_' +
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                const response = JSON.parse(xhr.responseText);
                if (response.after_checkout_url) {
                    $(".overlay").hide(); // hide loading image
                    console.log('UPLOAD SUCCESS');
                    var $idSubmit = $('#idSubmit');
                    $idSubmit.click();
                }
                else if (response.hasOwnProperty('errors')) {
                    $(".overlay").hide(); // hide loading image
                    console.log(response.errors[0]);
                    alert(response.errors[0]);
                }
                else {
                    $(".overlay").hide(); // hide loading image
                    alert("An unknown error occurred.. please try again later or contact sellerolympus@gmail.com");
                }
            }
            else {
                alert(xhr.error);
            }
        }
    };
    xhr.send();
}

// ID of user form, trigger google sheet action of appending row
$("#idForm").submit(function (e) {

    var url = "/append_google_sheet";

    $.ajax({
        type: "POST",
        url: url,
        data: $("#idForm").serialize()
    });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});

$(window).on('load', function () {
    var intervalFunc = function () {
        // set file name for user uploaded file
        $('#file-name').html($('#file-type').val().replace(/^.*\\/, ""));
    };

    $('#browse-click').on('click', function () {
        $('#file-type').click();
        setInterval(intervalFunc, 1);
        toolTip();
        uploadValidated = 1;

        // check if we are ready to enable submit
        enableSubmit();
        return false;
    });
});

// tool tip for disabled submit button
function toolTip() {
    var isDisabled = $('#mainSubmit').is(':disabled');
    if (isDisabled) {
        $('[data-toggle="tooltip"]').tooltip();
    }
}

function validatePassword() {
    var p = document.getElementById('form_password').value,
        errors = [];
    if (p.length < 8) {
        errors.push("Your password must be at least 8 characters");
    }
    if (p.search(/[a-z]/i) < 0) {
        errors.push("Your password must contain at least one letter.");
    }
    if (p.search(/[^A-Za-z0-9!@#$%\^&*]/) > 0) {
        errors.push("Your password can only contain numbers, letters, or the following: !@#$%^&*");
    }
    if (errors.length > 0) {
        passwordValidated = 0;
        document.activeElement.blur();
        alert(errors.join("\n"));
        return false;
    }

    passwordValidated = 1;
    enableSubmit();
    return true;
}

function validateFullName() {
    var p = document.getElementById('form_full_name').value,
        errors = [];
    if (p.length < 5) {
        errors.push("Your username must be at least 5 characters");
    }
    if (p.search(/[^A-Za-z0-9 ]/) > 0) {
        errors.push("Your username can only contain numbers, letters, or spaces");
    }
    if (errors.length > 0) {
        fullNameValidated = 0;
        document.activeElement.blur();
        alert(errors.join("\n"));
        return false;
    }

    fullNameValidated = 1;
    enableSubmit();
    return true;
}

function validateEmail() {
    var p = document.getElementById('form_email').value,
        errors = [];
    var email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email_regex.test(p) == false) {
        errors.push("Please enter a valid email");
    }
    if (errors.length > 0) {
        emailValidated = 0;
        document.activeElement.blur();
        alert(errors.join("\n"));
        return false;
    }

    emailValidated = 1;
    enableSubmit();
    emailAvailability();
    return true;
}

function enableSubmit() {

    if (emailValidated && passwordValidated && fullNameValidated && uploadValidated) {
        document.getElementById('mainSubmit').disabled = false;
    }

    else {
        document.getElementById('mainSubmit').disabled = true;
    }

}

/*
 Bind listeners when the page loads.
 */
(function () {
    toolTip();
    document.getElementById('mainSubmit').onclick = initUpload;
    document.getElementById('form_email').onblur = validateEmail;
    document.getElementById('form_password').onblur = validatePassword;
    document.getElementById('form_full_name').onblur = validateFullName;
})();
