var steps = [];
var testindex = 0;
var loadInProgress = false;//This is set to true when a page is still loading
var args = require('system').args;
var formFullName = args[1];
var formEmail = args[2];
var formPassword = args[3];

/*********SETTINGS*********************/
var webPage = require('webpage');
var page = webPage.create();
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';
page.settings.javascriptEnabled = true;
page.settings.loadImages = false;//Script is much faster with this field set to false
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;
/*********SETTINGS END*****************/

//console.log('All settings loaded, start with execution');
page.onConsoleMessage = function (msg) {
    //console.log(msg);
};

/**********DEFINE STEPS THAT FANTOM SHOULD DO***********************/
steps = [

    //Step 1 - Open Checkout page
    function () {
        //console.log('Step 1 - Open checkout page');
        page.open("http://sellerolympus.memberful.com/checkout?plan=22660", function (status) {
        });
    },

    //Step 2 - Populate and submit the login form
    function () {
        //console.log('Step 2 - Populate and submit the login form');
        page.evaluate(function (formFullName, formEmail, formPassword) {
            document.getElementById("checkout_full_name").value = formFullName;
            document.getElementById("checkout_email").value = formEmail;
            document.getElementById("checkout_password").value = formPassword;
            document.getElementById("checkout-form").submit();
        }, formFullName, formEmail, formPassword);
    },

    //Step 3 - Waiting to create account.
    function () {
        //console.log("Step 3 - Waiting to create account.");
        var result = page.evaluate(function () {
            return document.querySelectorAll("pre")[0].innerHTML;
        });
        console.log(result);
    },
];
/**********END STEPS THAT FANTOM SHOULD DO***********************/

//Execute steps one by one
interval = setInterval(executeRequestsStepByStep, 100);

function executeRequestsStepByStep() {
    if (loadInProgress == false && typeof steps[testindex] == "function") {
        //console.log("step " + (testindex + 1));
        steps[testindex]();
        testindex++;
    }
    if (typeof steps[testindex] != "function") {
        //console.log("test complete!");
        phantom.exit();
    }
}

// Listeners
page.onLoadStarted = function () {
    loadInProgress = true;
    //console.log('Loading started');
};
page.onLoadFinished = function () {
    loadInProgress = false;
    //console.log('Loading finished');
};
page.onConsoleMessage = function (msg) {
    //console.log(msg);
};
