var express = require('express');
var router = express.Router();
var requestify = require('requestify');

/* GET submit_to_memberful listing. */
router.get('/', function (req, res) {
    const formEmail = encodeURIComponent(req.query['form_email']);

    requestify.get('https://sellerolympus.memberful.com/email_availability?email=' + formEmail)
        .then(function (response) {
                // Get the response body (JSON parsed or jQuery object for XMLs)
                res.write(response.body);
                res.end();
            }
        );

});

module.exports = router;
