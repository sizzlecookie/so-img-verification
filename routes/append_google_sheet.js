var express = require('express');
var router = express.Router();
var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');


/* POST append_google_sheet listing. */
router.post('/', function (req, res, next) {

    // If modifying these scopes, delete the previously saved credentials
    // at /.credentials/sheets.googleapis.com-nodejs-quickstart.json
    var SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
    var TOKEN_DIR = './.credentials/';
    console.log('TOKEN: ' + TOKEN_DIR);
    var TOKEN_PATH = TOKEN_DIR + 'sheets.googleapis.com-nodejs-quickstart.json';

    // Load client secrets from a local file.
    fs.readFile('client_secret.json', function processClientSecrets(err, content) {
        if (err) {
            console.log('Error loading client secret file: ' + err);
            return;
        }
        // Authorize a client with the loaded credentials, then call the
        // Google Sheets API.
        authorize(JSON.parse(content), appendNewUser);
    });

    /**
     * Create an OAuth2 client with the given credentials, and then execute the
     * given callback function.
     */
    function authorize(credentials, callback) {
        var clientSecret = credentials.installed.client_secret;
        var clientId = credentials.installed.client_id;
        var redirectUrl = credentials.installed.redirect_uris[0];
        var auth = new googleAuth();
        var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

        // Check if we have previously stored a token.
        fs.readFile(TOKEN_PATH, function (err, token) {
            if (err) {
                getNewToken(oauth2Client, callback);
            } else {
                oauth2Client.credentials = JSON.parse(token);
                callback(oauth2Client);
            }
        });
    }

    /**
     * Get and store new token after prompting for user authorization, and then
     * execute the given callback with the authorized OAuth2 client.
     */
    function getNewToken(oauth2Client, callback) {
        var authUrl = oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES
        });
        console.log('Authorize this app by visiting this url: ', authUrl);
        var rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        rl.question('Enter the code from that page here: ', function (code) {
            rl.close();
            oauth2Client.getToken(code, function (err, token) {
                if (err) {
                    console.log('Error while trying to retrieve access token', err);
                    return;
                }
                oauth2Client.credentials = token;
                storeToken(token);
                callback(oauth2Client);
            });
        });
    }

    /**
     * Store token to disk be used in later program executions.
     */
    function storeToken(token) {
        try {
            fs.mkdirSync(TOKEN_DIR);
        } catch (err) {
            if (err.code != 'EEXIST') {
                throw err;
            }
        }
        fs.writeFile(TOKEN_PATH, JSON.stringify(token));
        console.log('Token stored to ' + TOKEN_PATH);
    }

    /**
     * Output name, email, verification image to google sheet
     */
    function appendNewUser(auth) {
        var sheets = google.sheets('v4');
        sheets.spreadsheets.values.append({
            auth: auth,
            spreadsheetId: '1fMKdrik0iwGp73x1xpM-j2PmAgCdrRDho39gA90OvG4',
            range: 'Sheet1!A1:H1',
            valueInputOption: 'USER_ENTERED',
            resource: {
                range: 'Sheet1!A1:H1',
                majorDimension: 'ROWS',
                values: [
                    [
                        req.body.fullName,
                        req.body.emailAddress,
                        "https://s3-us-west-2.amazonaws.com/sellerolympus-img-verification/"
                        + req.body.fullName.replace(/\s/g, "+") + "_" + req.body.emailAddress
                    ]
                ]
            }
        }, function (err, response) {
            if (err) {
                console.log('The API returned an error: ' + err);
            }
        });
    }

    res.send(req.body.zip);

});

module.exports = router;
