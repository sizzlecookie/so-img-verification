var express = require('express');
var router = express.Router();
var aws = require('aws-sdk');
aws.config.region = 'us-west-2';
var S3_BUCKET = process.env.S3_BUCKET;

/* GET s3_img_upload_signed_request listing. */
router.get('/', function (req, res) {
    const s3 = new aws.S3();
    const fileName = req.query['file-name'];
    const fileType = req.query['file-type'];
    console.log(fileName);
    const s3Params = {
        Bucket: S3_BUCKET,
        Key: fileName,
        Expires: 60,
        ContentType: fileType,
        ACL: 'public-read'
    };
    s3.getSignedUrl('putObject', s3Params, function (err, data) {
        if (err) {
            console.log(err);
            return res.end();
        }
        const returnData = {
            signedRequest: data,
            url: 'https://' + S3_BUCKET + '.s3.amazonaws.com/' + fileName
        };
        res.write(JSON.stringify(returnData));
        res.end();
    });
});

module.exports = router;
