var express = require('express');
var router = express.Router();

/* GET submit_to_memberful listing. */
router.get('/', function (req, res) {

    //executing-phantom.js
    const formFullName = req.query['form_full_name'].replace(/\n$/, '');
    const formEmail = req.query['form_email'].replace(/\n$/, '');
    const formPassword = req.query['form_password'].replace(/\n$/, '');

    var spawn = require('child_process').spawn;
    var args = ["./scripts/phantom-script.js", formFullName, formEmail, formPassword];

    // If phantom is in path use 'phantomjs', otherwise provide the path to the phantom executable
    var phantomExecutable = 'phantomjs';
    var successFlag;
    var failFlag;
    var jsonObj;

    //converts a Uint8Array to its string representation
    function Uint8ArrToString(myUint8Arr) {
        return String.fromCharCode.apply(null, myUint8Arr);
    };

    var child = spawn(phantomExecutable, args);

    // Receive output of the child process
    child.stdout.on('data', function (data) {
        var textData = Uint8ArrToString(data);

        //console.log(textData);
        try {
            console.log("parsing....'" + textData + "'");
            jsonObj = JSON.parse(textData);
            if (jsonObj.after_checkout_url) {
                //console.log("Successfully parsed!");
                successFlag = textData;
            }
            else if (jsonObj.errors[0]) {
                //replace this error msg w/ a more appropriate one
                failFlag = textData.replace(/It looks like you already have an account. To complete this order, please sign in above./, "Account exists already! Log in at sellerolympus.com or email sellerolympus@gmail.com for support");
            }
        } catch (e) {
            console.log(e);
        }

    });

    // Receive error output of the child process
    child.stderr.on('data', function (err) {
        var textErr = Uint8ArrToString(err);
        console.log(textErr);
    });

    // Triggered when the process closes
    child.on('close', function (code) {
        console.log('Process closed with status code: ' + code);
        if (successFlag) {
            res.write(successFlag);
            res.end();
        } else {
            if (failFlag) {
                res.write(failFlag);
                res.end();
            } else {
                res.write('{"errors":["An unknown error occurred (submit_to_memberful.js).. please try again later or contact sellerolympus@gmail.com"]}');
                res.end();
            }
        }
    });
});

module.exports = router;
